'use strict';


// Declare app level module which depends on filters, and services
angular.module('ywe', ['ywe.filters', 'ywe.services', 'ywe.directives', 'ywe.controllers']).
    config(function ($compileProvider) {
        $compileProvider.urlSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
    }).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'tpl/home.html',
                controller: 'HomeCtrl'
            })
            .when('/set', {
                templateUrl: 'tpl/set.html',
                controller: 'SetCtrl'
            })
            .when('/six', {
                templateUrl: 'tpl/canvas.html',
                controller: 'SixCtrl'
            })
            .otherwise({redirectTo: '/'});
    }]);
