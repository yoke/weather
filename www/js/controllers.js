'use strict';

/* Controllers */

angular.module('ywe.controllers', []).
  controller('HomeCtrl', ['$scope', 'weather', function ($scope, weather) {

    var lastTime;
    $scope.date = moment().format('LL');

    function getData() {
      var now = moment();
      $scope.last = now.format('MMMM DD, HH:mm:ss');
      if (lastTime && now.diff(lastTime, 'hours') < 1) {
        return;
      }
      lastTime = now;
      weather.sk.get({ID: '101210101'}, function (skStr) {
        $scope.sk = eval(skStr).weatherinfo;
      });
      weather.info.get({ID: '101210101'}, function (infoStr) {
        $scope.info = eval(infoStr).weatherinfo;
      });
    }

    $scope.refreshData = function () {
      getData();
    };

    getData();

  }]).
  controller('SixCtrl', [function () {

  }]).
  controller('SetCtrl', [function () {

  }]).
  controller('MainCtrl', ['$scope',function ($scope) {
    $scope.subnavShow = false;
    $scope.showMenu = function () {
      $scope.subnavShow = !$scope.subnavShow;
    };
    $scope.hideMenu = function () {
      $scope.subnavShow = false;
    };
  }]);