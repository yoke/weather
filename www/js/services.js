'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('ywe.services', ['ngResource']).
    factory('weather', ['$resource', function ($resource) {
        return {
            sk: $resource('http://www.weather.com.cn/data/sk/:ID.html'),
            info: $resource('http://www.weather.com.cn/data/cityinfo/:ID.html'),
            six: $resource('http://m.weather.com.cn/data/:ID.html')
        }
    }]);